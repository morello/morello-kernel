#!/bin/sh

set -ex

# Install toolchain
curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo /tmp/toolchain.zip \
  https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/main/download?job=build-toolchain
unzip /tmp/toolchain.zip -d /tmp
tar -xf /tmp/morello-clang.tar.xz -C ${TOOLS_DIR}
export PATH=${TOOLS_DIR}/clang-current/bin/:${PATH}
clang -v

# Get the kernel source code
KERNEL_BRANCH="morello/master"
git ls-remote https://git.morello-project.org/morello/kernel/linux.git ${KERNEL_BRANCH}
git clone --depth=1 https://git.morello-project.org/morello/kernel/linux.git -b ${KERNEL_BRANCH}

# Get the kernel config
curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo linux/arch/arm64/configs/morello_kernel_defconfig \
https://git.morello-project.org/morello/morello-sdk/-/raw/morello/mainline/morello/projects/config/morello_kernel_defconfig
sha256sum linux/arch/arm64/configs/morello_kernel_defconfig

# Build the kernel
make -C linux ARCH=arm64 LLVM=1 LLVM_IAS=1 morello_kernel_defconfig
make -C linux ARCH=arm64 LLVM=1 LLVM_IAS=1 -j$(nproc)

# Publish artifact
cp -a linux/arch/arm64/boot/Image ${CI_PROJECT_DIR}/Image.dev
