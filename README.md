# Morello Kernel Image
This project provides a binary kernel image that can be used on Morello SoCs or Morello FVP.

## Latest Image
* [Latest Kernel Image for Morello](https://git.morello-project.org/morello/morello-kernel/-/jobs/artifacts/main/raw/Image.dev?job=build-kernel)
